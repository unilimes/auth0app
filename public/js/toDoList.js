function DeleteItem(val) {

        console.log(val);

        $.ajax({
            type: "post",
            url: "http://localhost:3000/user",
            data: JSON.stringify({task: val, action: 'delete'}),
            dataType: "json",
            contentType: "application/json"
        });

        $( "label:contains(" + val + ")" ).closest('li').remove();


}

document.addEventListener("DOMContentLoaded", function(event) {


    document.onkeydown = function(e) {
        e = e || window.event;

        console.log();

        if(e.keyCode == 13 && document.hasFocus() && !/^\s*$/.test($('#input').val())){

            var label = document.createElement('label');
            label.innerHTML = $('#input').val();

            var li = document.createElement('li');
            li.onclick = function () {
                DeleteItem(label.innerHTML);
            };
            li.className = 'ui-state-default';
            li.id = 'item';
            var div = document.createElement('div');
            div.className = 'checkbox';


            li.appendChild(div);
            div.appendChild(label);

            document.getElementById('sortable').appendChild(li);

            $('#input').val('');

            $.ajax({
                type: "post",
                url: "http://localhost:3000/user",
                data: JSON.stringify({task: label.innerHTML, action: 'add'}),
                dataType: "json",
                contentType: "application/json"
            });

        }

    };




});




