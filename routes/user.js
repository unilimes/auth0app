const express = require('express');
const passport = require('passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const router = express.Router();
const User = require('../mongo/user');
const ToDo = require('../mongo/ToDo');
var bodyParser = require("body-parser");

var app = express();

var userEmail = 'none@gmail.com';


router.get('/userinfo', ensureLoggedIn, (req, res, next) => {


    User.find({email: req.query.email}, (err, user)=>{
        if(err) throw err;

        console.log('USER',user[0]);
        console.log('USER',user[0].email);


        res.render('userInfo', {
            user: user[0]
        });

    });



});

router.post('/userinfo', ensureLoggedIn, (req, res) => {

    console.log(req.body);

    const field = req.body.field;
    const content = {};
	content[field]= req.body.content;
    User.findOneAndUpdate({email: req.body.email},  content,  (err, user)=> {
		if (err) throw err;

		console.log(user);
	});



});



router.post('/', function (req, res) {

    console.log(req.body);

    if(req.body.action == 'add') {
        ToDo.findOneAndUpdate({email: userEmail}, {$push: {ToDo: req.body.task}}, function (err, user) {
            if (err) throw err;
        });
    }else if(req.body.action == 'delete'){
        ToDo.update({email: userEmail}, {$pull: {ToDo: req.body.task}}, function (err, user) {
            if (err) throw err;
        });
    }

});

router.get('/', ensureLoggedIn, (req, res, next) => {

    var items = [];

    User.find({}, function(err, users) {
        if (err) throw err;

        User.find({email: req.user.displayName}, (err, user)=>{


            if(err) throw err;

            userEmail = req.user.displayName;

            if(user.length == 0){

                var newUser = User({
                    email: req.user.displayName,
                    nickname: req.user.nickname,
                    role: req.user._json['https://example.com/roles'],
                    description: 'This is Default Description about out User',
                    avatar: req.user.picture
                });

                var ToDoList = ToDo({
                    email: req.user.displayName
                });


                ToDoList.save(function(err) {
                    if (err) throw err;
                });

                newUser.save(function(err) {
                    if (err) throw err;
                });


                res.render('user', {
                    user: req.user,
                    items: items,
                    userList: users
                });
            }
            else{

                ToDo.find({ email: req.user.displayName }, function(err, todo) {
                    if (err) throw err;

                    items = todo[0].ToDo;

                    res.render('user', {
                        user: req.user,
                        items: items,
                        userList: users
                    });
                });

            }


        });
    });







});



module.exports = router;
