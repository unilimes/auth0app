var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/27017');

var Schema = mongoose.Schema;


var userSchema = new Schema({
    email: String,
    nickname: String,
    role: String,
    description: String,
    avatar: String
});


var User = mongoose.model('User', userSchema);


module.exports = User;